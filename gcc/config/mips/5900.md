;; Copyright (C) 2002-2015 Free Software Foundation, Inc.
;;
;; This file is part of GCC.
;;
;; GCC is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; GCC is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.
;;
;; R5900 instruction patterns and pipeline tuning.

(define_automaton "r5900_alu,r5900_fpu,r5900_br,r5900_ls")

(define_cpu_unit "r5900_alu0,r5900_alu1" "r5900_alu")
(define_cpu_unit "r5900_c1" "r5900_fpu")
(define_cpu_unit "r5900_br" "r5900_br")
(define_cpu_unit "r5900_ls" "r5900_ls")

(define_insn_reservation "r5900_alu" 1
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "unknown,prefetch,prefetchx,condmove,const,arith,
			shift,slt,clz,trap,multi,nop,logical,signext,move"))
 "r5900_alu0|r5900_alu1")

(define_insn_reservation "r5900_loadstore" 1
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "load,store"))
 "r5900_ls")

(define_insn_reservation "r5900_fploadstore" 2
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "fpload,fpstore"))
 "r5900_c1,nothing")

(define_insn_reservation "r5900_fcvt" 4
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "fcvt"))
 "r5900_c1,nothing*3")

(define_insn_reservation "r5900_fmove" 4
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "fabs,fneg,fmove"))
 "r5900_c1,nothing*3")

(define_insn_reservation "r5900_fcmp" 4
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "fcmp"))
 "r5900_c1,nothing*3")

(define_insn_reservation "r5900_fadd" 4
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "fadd"))
 "r5900_c1,nothing*3")

(define_insn_reservation "r5900_fmul_single" 4
  (and (eq_attr "cpu" "r5900")
       (and (eq_attr "type" "fmul,fmadd")
            (eq_attr "mode" "SF")))
 "r5900_c1,nothing*3")

(define_insn_reservation "r5900_fdiv_single" 8
  (and (eq_attr "cpu" "r5900")
       (and (eq_attr "type" "fdiv,frdiv")
            (eq_attr "mode" "SF")))
 "r5900_c1*7,nothing")

(define_insn_reservation "r5900_fsqrt_single" 8
  (and (eq_attr "cpu" "r5900")
       (and (eq_attr "type" "fsqrt")
            (eq_attr "mode" "SF")))
 "r5900_c1*7,nothing")

(define_insn_reservation "r5900_frsqrt_single" 14
  (and (eq_attr "cpu" "r5900")
       (and (eq_attr "type" "frsqrt")
            (eq_attr "mode" "SF")))
 "r5900_c1*13,nothing")

(define_insn_reservation "r5900_xfer" 2
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "mfc,mtc"))
 "r5900_c1+r5900_ls,nothing")

(define_insn_reservation "r5900_branch" 1
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "branch,jump,call"))
 "r5900_br")

(define_insn_reservation "r5900_hilo" 1
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "mfhi,mflo,mthi,mtlo"))
 "r5900_alu0")

(define_insn_reservation "r5900_imul" 4
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "imul,imul3,imadd"))
 "r5900_alu0*2,nothing*2")

(define_insn_reservation "r5900_idiv" 37
  (and (eq_attr "cpu" "r5900")
       (eq_attr "type" "idiv,idiv3"))
 "r5900_alu0*37")

;; RSQRT
(define_insn "rsqrtsf"
  [(set (match_operand:SF 0 "register_operand" "=f")
	(div:SF (match_operand:SF 1 "register_operand" "f")
		  (sqrt:SF (match_operand:SF 2 "register_operand" "f"))))]
  "TARGET_MIPS5900"
  "rsqrt.s\t%0,%1,%2"
  [(set_attr "type" "frsqrt")
   (set_attr "mode" "SF")])

;; MIN and MAX
(define_insn "sminsf3"
  [(set (match_operand:SF 0 "register_operand" "=f")
	(smin:SF (match_operand:SF 1 "register_operand" "f")
		 (match_operand:SF 2 "register_operand" "f")))]
  "TARGET_MIPS5900"
  "min.s\t%0,%1,%2"
  [(set_attr "type" "fadd")
   (set_attr "mode" "SF")])

(define_insn "smaxsf3"
  [(set (match_operand:SF 0 "register_operand" "=f")
	(smax:SF (match_operand:SF 1 "register_operand" "f")
		 (match_operand:SF 2 "register_operand" "f")))]
  "TARGET_MIPS5900"
  "max.s\t%0,%1,%2"
  [(set_attr "type" "fadd")
   (set_attr "mode" "SF")])
